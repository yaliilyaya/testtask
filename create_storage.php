<?php
function getName() {
    $name_list = ["Илья","Андрей","Сергей","Алексей","Николай","Василий","Максим"];
    return $name_list[rand(0, count($name_list)-1)];
}
function getSurname() {
    $surname_list = ["Смирнов","Путин","Генералдов","Сироткин"];
    return $surname_list[rand(0, count($surname_list)-1)];
}
function getPatronymic() {
    $patronymic_list = ["Владимиирович","Андреевич","Николаич", ""];
    return $patronymic_list[rand(0, count($patronymic_list)-1)];
}
function getDR() {
    $time = strtotime(rand(1, 30).".".rand(1, 12).".".rand(1970, 1995));
    return date("d.m.Y", $time);
}
function getPhone() {
    return "+7(90".rand(1, 9).") ".rand(100, 999)." ".rand(10, 99)." ".rand(10, 99);
}

$data = [
    'func_list' => [
        'func_1' => [
            'name' => "Руководитель"
        ],
        'func_2' => [
            'name' => "Менеджер"
        ],
        'func_3' => [
            'name' => "Приёмщик"
        ],
        'func_4' => [
            'name' => "Кладовщик"
        ],
        'func_5' => [
            'name' => "Охранник"
        ],
    ],
    'place_list' => [
        'place_1' => [
            'name' => 'Офис',
            'func' => [1, 2],
            'worker' => [1,2,3]
        ],
        'place_2' => [
            'name' => 'Склад',
            'func' => [3, 4],
            'worker' => [4,5,6]
        ],
        'place_3' => [
            'name' => 'Охрана',
            'func' => [5],
            'worker' => [7,8]
        ],
    ],
    'worker_list' => [
        'worker_1' => [
            'name' => getName(),
            'surname' => getSurname(),
            'patronymic' => getPatronymic(),
            'date' => getDR(),
            'func' =>  1,
            'place' => 1,
            'phone' => getPhone(),
            'status' => "work"
        ],
        'worker_2' => [
            'name' => getName(),
            'surname' => getSurname(),
            'patronymic' => getPatronymic(),
            'date' => getDR(),
            'func' =>  2,
            'place' => 1,
            'phone' => getPhone(),
            'status' => "work"
        ],
        'worker_3' => [
            'name' => getName(),
            'surname' => getSurname(),
            'patronymic' => getPatronymic(),
            'date' => getDR(),
            'func' =>  2,
            'place' => 1,
            'phone' => getPhone(),
            'status' => "fired"
        ],
        'worker_4' => [
            'name' => getName(),
            'surname' => getSurname(),
            'patronymic' => getPatronymic(),
            'date' => getDR(),
            'func' =>  3,
            'place' => 2,
            'phone' => getPhone(),
            'status' => "work"
        ],
        'worker_5' => [
            'name' => getName(),
            'surname' => getSurname(),
            'patronymic' => getPatronymic(),
            'date' => getDR(),
            'func' =>  4,
            'place' => 2,
            'phone' => getPhone(),
            'status' => "work"
        ],
        'worker_6' => [
            'name' => getName(),
            'surname' => getSurname(),
            'patronymic' => getPatronymic(),
            'date' => getDR(),
            'func' =>  4,
            'place' => 2,
            'phone' => getPhone(),
            'status' => "fired"
        ],
        'worker_7' => [
            'name' => getName(),
            'surname' => getSurname(),
            'patronymic' => getPatronymic(),
            'date' => getDR(),
            'func' =>  5,
            'place' => 3,
            'phone' => getPhone(),
            'status' => "work"
        ],
        'worker_8' => [
            'name' => getName(),
            'surname' => getSurname(),
            'patronymic' => getPatronymic(),
            'date' => getDR(),
            'func' =>  5,
            'place' => 3,
            'phone' => getPhone(),
            'status' => "fired"
        ],
    ],
];
file_put_contents("/var/www/test_task/app/data.json", json_encode($data));