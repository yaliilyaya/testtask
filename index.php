<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/css/main.css">
<script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="/js/jquery.storageapi.min.js"></script>
<script type="text/javascript" async="" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" async="" src="/js/jquery.mask.min.js"></script>
<script type="text/javascript" async="" src="/app/init.js"></script>

<div id="window_manager"></div>
<div id="menu" class="btn-group input-group"></div>
