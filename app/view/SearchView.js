function SearchView() {

    this.table = null;
    this.body = null;
    this.init = function() {
        this.table = $('<table>', {'class': 'table table-hover table-striped table-search'})
            .append(this.body = $('<tbody>'));
        $('#menu').append(this.table)
    };
    this.render = function (worker_list) {
        this.body.empty();
        for(var i in worker_list) {
            var worker = worker_list[i];
            this.body.append($('<tr>')
                    .append([
                        $('<td>').html(worker.fullName()),
                        $('<td>').html(worker.date),
                        $('<td>').html(worker.func)
                    ])
                );
        }
    };
    this.empty = function() {
        this.body.empty();
    }
}