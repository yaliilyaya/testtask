function TableView() {
    var t = this;
    this.table = null;
    this.body = null;
    this.page_nav = null;

    this.field = {
        fio: null,
        place: null,
        status: null
    };

    this.init = function() {
        this.table = $('<table>', {'class': 'table table-hover table-striped'})
            .append($('<thead>')
                .append($('<tr>')
                    .append([
                        this.field.fio = $('<th>')
                            .html("Фамилия, Имя, Отчество")
                            .append($('<i>', {'class': "glyphicon"})),
                        $('<th>').html("Дата рождения"),
                        this.field.place = $('<th>').html("Отдел")
                            .append($('<i>', {'class': "glyphicon"})),
                        $('<th>').html("Должность"),
                        $('<th>').html("Телефон"),
                        this.field.status = $('<th>').html("Статус")
                            .append($('<i>', {'class': "glyphicon"})),
                        $('<th>')
                ])
            ))
            .append(this.body = $('<tbody>'));
        $('body').append(this.table);
        this.field.fio.click(function() {
            t.controller.setSort('fio');
            t.changeFilter($(this));
        });
        this.field.place.click(function() {
            t.controller.setSort('place');
            t.changeFilter($(this));
        });
        this.field.status.click(function() {
            t.controller.setSort('status');
            t.changeFilter($(this));
        });
        this.table.after(
            this.page_nav = $('<ul>', {'class': "pagination"})
        );
    };
    this.render = function(data) {
        this.body.empty();
        for(var i in data.worker_list) {
            var worker = data.worker_list[i];
            this.body.append($('<tr>')
                    .append($('<td>').html(worker.fullName()))
                    .append($('<td>').html(worker.date))
                    .append($('<td>').html(worker.place_name))
                    .append($('<td>').html(worker.func_name))
                    .append($('<td>').html(worker.phone))
                    .append($('<td>').html(worker.status == "work" ? "Работает" : "Уволен"))
                    .append($('<td>')
                        //.append(this.createBtn('pencil', null))
                        .append(this.createBtn('th-list', null)
                            .click(function(worker){return function() {
                                console.debug(worker.id);
                                ChangePlaceController.changePlace(worker.id);
                            }}(worker))
                        )
                        .append(this.createBtnChengeStatus(worker))
                    )
            )
        }
        this.renderPageNav(data.count_page, data.page);
    };
    this.renderPageNav = function(count, page) {
        this.page_nav.empty();
        for (var i = 1; i <= count; i++) {
            var li = $('<li>')
                .append($('<a>', {})
                    .html(i)
                    .click(function(page) {return function (){
                        t.controller.changePage(page);
                    }}(i))
            );

            if (i == page) {
                li.addClass("active");
            }
            this.page_nav.append(li)
        }
    };

    this.changeFilter = function(th) {
        $('thead th i', this.table).removeClass("glyphicon-chevron-down");
        $('i', th).addClass("glyphicon-chevron-down");
    };

    this.createBtnChengeStatus = function(worker) {
        var type = 'upload';
        if (worker.status == 'work') {
            type = "trash";
        }
        return this.createBtn(type, function() {
            if (worker.status == 'work') {
                worker.setStatusFired();
            }
            else {
                worker.setStatusWork();
            }
            TableController.update();
        });
    };
    this.createBtn = function(icon, claback) {
        return $('<button>', {'class': "btn btn-default"})
                    .append($('<i>', {'class': "glyphicon glyphicon-"+ icon}))
                    .click(claback);
    }
}
