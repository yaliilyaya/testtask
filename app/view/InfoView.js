function InfoView() {
    var t = this;
    this.controller = null;
    this.widow = null;
    this.body = null;
    this.init = function() {
        var body;

        this.widow = $('<div>', {'class': "modal-dialog hidden modal-info"})
            .append($('<form>', {'class': "modal-content"})
                .append($('<div>', {'class': "modal-header"})
                    .append($('<button>', {type: "button",
                        'class': "close",
                        'data-dismiss': "modal",
                        'aria-label': "Close"
                    })
                        .click(function() {
                            t.close();
                        })
                        .append($('<span>', {'aria-hidden': "true"}).html("&times;"))
                )
                    .append($('<h4>', {'class': "modal-title"}).html("Информация по заданию"))
            )
                .append(this.body = $('<div>', {'class': "modal-body"}))
                .append($('<div>', {'class': "modal-footer"})
                    .append($('<button>', {type: "button",
                        'class': "btn btn-default",
                        'data-dismiss': "modal"
                    })
                        .html("Закрыть")
                        .click(function() {
                            t.close();
                        })
                    )
                )
            );

        $('body').prepend(this.widow);
    };

    this.open = function() {
        this.widow.removeClass('hidden');
    };

    this.close = function() {
        this.widow.addClass('hidden');
    };


    this.render = function(data) {
        if(this.body != null) {
            ViewComponent.getTemplate("Info", function(d) {
                t.body.append(d);
                t.body = null;
            });


        }
    };

}