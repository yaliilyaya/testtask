function AddWorkerView() {
    var t = this;
    this.controller = null;
    this.widow = null;
    this.body = null;

    this.init = function() {
        var body,
            field_list = this.controller.field_list;

        this.widow = $('<div>', {'class': "modal-dialog hidden"})
            .append($('<form>', {'class': "modal-content"})
                .append($('<div>', {'class': "modal-header"})
                    .append($('<button>', {type: "button",
                                            'class': "close",
                                            'data-dismiss': "modal",
                                            'aria-label': "Close"
                        })
                        .click(function() {
                            t.close();
                        })
                        .append($('<span>', {'aria-hidden': "true"}).html("&times;"))
                    )
                    .append($('<h4>', {'class': "modal-title"}).html("Добавление сотрудника"))
                )
                .append(body = $('<div>', {'class': "modal-body"}))
                .append($('<div>', {'class': "modal-footer"})
                    .append($('<button>', {type: "button",
                                        'class': "btn btn-default",
                                        'data-dismiss': "modal"
                    })
                        .html("Закрыть")
                        .click(function() {
                            t.close();
                        })
                    )
                    .append($('<button>', {type: "button",
                                        'class': "btn btn-primary",
                                        'data-dismiss': "modal"
                    })
                        .html("Сохранить")
                        .click(function() {
                            t.controller.save();
                        })
                    )
                )
            );

        body.append([
            this.addRow("Фамилия, Имя, Отчество",
                $('<div>').append([
                    field_list.surname = $('<input>', {type:"text", "placeholder": "Фамилия"}),
                    field_list.name = $('<input>', {type:"text", "placeholder": "Имя"}),
                    field_list.patronymic = $('<input>', {type:"text", "placeholder": "Отчество"})
                ])).addClass('form_field_fio')
            ,
            this.addRow("Дата рождения",
                field_list.date = $('<input>', {type:"text"})
            ),
            this.addRow("Отдел", field_list.place = $('<select>')),
            this.addRow("Должность", field_list.func = $('<select>')),

            this.addRow("Мобильный телефон", field_list.phone = $('<input>', {type:"text"})),

            this.addRow("Статус", field_list.status = $('<select>'))
        ]);
        $('input, select', body).addClass("form-control");

        field_list.date.mask("99.99.9999");
        field_list.phone.mask("+7(999) 999 99 99");

        field_list.place.change(function() {
            t.controller.refreshFieldFunc();
        });
        $('input', body).keydown(function() {

            var field = $(this);
            if (field.hasClass('has-error')) {
                field.removeClass('has-error');
                field.removeAttr('title');
                field.tooltip( "destroy" );
            }
            else if (field.parent().hasClass('has-error')) {
                field.parent().removeClass('has-error');
                field.removeAttr('title');
                field.tooltip( "destroy" );
            }
        });
        $('body').prepend(this.widow);
    };

    this.open = function() {
        this.widow.removeClass('hidden');
    };

    this.close = function() {
        this.widow.addClass('hidden');
    };
    this.fieldSetError = function(field, mess) {

        if ($.inArray(field, ["name", "surname", "patronymic"]) >= 0) {
            this.controller.field_list[field].addClass("has-error");
        }
        else {
            this.controller.field_list[field].parent().addClass("has-error");
        }
        this.controller.field_list[field].attr('title', mess);
        this.controller.field_list[field].tooltip();
    };

    this.render = function(data) {
        if (data['place_list'] != null) {
            this.controller.field_list.place.empty();
            for(var i in data.place_list) {
                var place = data.place_list[i];
                this.controller.field_list.place.append($('<option>').html(place.name).val(place.getNumber()));
            }
        }
        if (data['func_list'] != null) {
            this.controller.field_list.func.empty();
            for (var i in data.func_list) {
                var func = data.func_list[i];
                this.controller.field_list.func.append($('<option>').html(func.name).val(func.getNumber()));
            }
        }
        if (data['status_list'] != null) {
            this.controller.field_list.status.empty();
            for (var i in data.status_list) {
                var status = data.status_list[i];
                this.controller.field_list.status.append($('<option>').html(status.name).val(status.value));
            }
        }
    };
    this.addRow = function(name, field) {
        field.attr('placeholder', name);

        return $('<div>', {'class': "form-group"})
            .append($('<label>').html(name))
            .append(field);
    };


}
