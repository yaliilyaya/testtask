function ChangePlaceView() {
    var t = this;
    this.controller = null;
    this.widow = null;
    this.body = null;
    this.field_list = {
        worker: null,
        place: null,
        func: null
    };
    this.init = function() {
        var body;
        this.controller.field_list = this.field_list;
        this.widow = $('<div>', {'class': "modal-dialog hidden"})
            .append($('<form>', {'class': "modal-content"})
                .append($('<div>', {'class': "modal-header"})
                    .append($('<button>', {type: "button",
                        'class': "close",
                        'data-dismiss': "modal",
                        'aria-label': "Close"
                    })
                        .click(function() {
                            t.close();
                        })
                        .append($('<span>', {'aria-hidden': "true"}).html("&times;"))
                )
                    .append($('<h4>', {'class': "modal-title"}).html("Перевод сотрудниека между отделениями"))
            )
                .append(this.body = $('<div>', {'class': "modal-body"}))
                .append($('<div>', {'class': "modal-footer"})
                    .append($('<button>', {type: "button",
                        'class': "btn btn-default",
                        'data-dismiss': "modal"
                    })
                        .html("Закрыть")
                        .click(function() {
                            t.close();
                        })
                    )
                    .append($('<button>', {type: "button",
                        'class': "btn btn-primary",
                        'data-dismiss': "modal"
                    })
                        .html("Сохранить")
                        .click(function() {
                            t.controller.save();
                        })
                    )
                )
            );
        this.body.append([
            this.field_list.worker = $('<input>', {type: "hidden"}),
            this.addRow("Отдел", this.field_list.place = $('<select>')),
            this.addRow("Должность", this.field_list.func = $('<select>'))
        ]);

        this.field_list.place.change(function() {
            t.controller.refreshFieldFunc();
        });

        $('input, select', this.body).addClass("form-control");
        $('body').prepend(this.widow);
    };

    this.open = function() {
        this.widow.removeClass('hidden');
    };

    this.close = function() {
        this.widow.addClass('hidden');

    };

    this.render = function(data) {
        if (data['worker_id'] != null) {
            this.field_list.worker.val(data['worker_id']);
        }
        if (data['place_list'] != null) {
            this.field_list.place.empty();
            for(var i in data.place_list) {
                var place = data.place_list[i];
                this.field_list.place.append($('<option>').html(place.name).val(place.getNumber()));
            }
        }
        if (data['func_list'] != null) {
            this.field_list.func.empty();
            for (var i in data.func_list) {
                var func = data.func_list[i];
                this.field_list.func.append($('<option>').html(func.name).val(func.getNumber()));
            }
        }

    };
    this.addRow = function(name, field) {
        field.attr('placeholder', name);

        return $('<div>', {'class': "form-group"})
            .append($('<label>').html(name))
            .append(field);
    };

}