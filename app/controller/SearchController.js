/**
 * Контроллер выполняет какие либо монипуляции с данными и выводит их через Представление
 * Контроллер отвечает за поиск работника по ФИО и вывод подсказок по найденным результатам.
 * При нажатии кнопки "Найти" функция фильтрации передаётся в TableController
 * @property view Представление контроллера
 * @type {SearchController}
 */
var SearchController = new function() {
    var t = this;
    this.filter = {
        limit:3
    };

    this.field = null;
    this.btn = null;
    this.init = function() {
        ViewComponent.getView("Search", this, function() {

        });
        //Добавляем кнопки и поле для поиска в меню
        $('#menu')
            .prepend([
                $('<span>', {'class': "input-group-addon"})
                    .append($('<i>', {'class': "glyphicon glyphicon-search"})),
                this.field = $('<input>', {type: "text", 'class': "form-control", placeholder: "Поиск"})
                    .css({float:"left", width: '300px'})
                    .keyup(function() {
                        t.search(t.field.val() + "");
                    }),
                this.btn = $('<button>', {type: 'button', 'class': "btn btn-default"})
                    .html("Найти")
                    .click(function() {
                        t.click();
                    })
            ]);
    };
    /**
     * Осуществляет поиск работников по заданной фразе
     * @param text
     */
    this.search = function(text) {
        text = text.replace(/[^А-Яа-я\s]+/g, "");//Вырезаем все символы которые могут вызвать ошибку
        if (text.length <= 0) {
            this.view.empty();
            this.filter.check = null;
            return;
        }
        this.filter.check = function(worker) {//Задаём функцию фильтрации
            var name = worker.surname + " " + worker.name + " " +  worker.patronymic;
            return (name.search(text) != -1);
        };
        var worker_list = WorkerModel.getList(this.filter);
        this.func_list = StorageComponent.getFuncList();
        for (var i in worker_list) { //Заменяем ID должности на имя
            var worker = worker_list[i],
                func_id = "func_" + worker.func;
            worker.func = (this.func_list[func_id] != null) ? this.func_list[func_id].name : "";
        }
        this.view.render(worker_list);
    };
    /**
     * передаём функцию фильтрации таблице
     */
    this.click = function() {
        TableController.setFilter(this.filter.check);
        this.view.empty();
    };
};