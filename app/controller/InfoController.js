/**
 * Контроллер выполняет какие либо монипуляции с данными и выводит их через Представление.
 * Контроллер отвечает за вывод информации о задании и сведения о результате его выполнения.
 *
 * @property view Представление контроллера
 * @type {InfoController}
 */
var InfoController = new function() {
    var t = this;
    this.view = null;
    this.init = function() {
        ViewComponent.getView("Info", this, function() {
            //Добавляем кнопку в меню
            $('#menu').append($('<button>', {type: 'button', 'class': "btn btn-default"})
                .html("Задание")
                .click(function() {
                    t.open();
                }));
            t.view.render();
        });
    };
    this.open = function() {
        this.view.open();
    }
};