/**
 * Контроллер выполняет какие либо монипуляции с данными и выводит их через Представление
 * Контроллер отвечает за измениение Отдела и должности у работника
 * @property view Представление контроллера
 * @type {ChangePlaceController}
 */
var ChangePlaceController = new function() {
    var t = this;
    this.view = null;
    this.field_list = {};
    this.init = function() {
        ViewComponent.getView("ChangePlace", this, function() {

        });
    };

    /**
     * Сохраняет у работника новый отдел и должность
     */
    this.save = function() {
        var worker = WorkerModel.getWorker(this.field_list.worker.val());
        if (worker == null) {
            this.view.close();
            return;
        }

        worker.place = parseInt(this.field_list.place.val());
        worker.func = parseInt(this.field_list.func.val());
        worker.save();

        //console.debug(worker);

        TableController.update();
        this.view.close();
    };
    /**
     * Выводит форму для перевода работника в другой отдел
     * @param worker_id
     */
    this.changePlace = function(worker_id) {
        this.open();
        this.view.render({
            worker_id: worker_id,
            place_list: PlaceModel.getList(),
            func_list: this.getFieldFuncList()
        });
    };
    this.open = function() {
        this.view.open();
    };
    /**
     * При изменении отдела в форме нужно загрузить список должностей для этого отдела
     */
    this.refreshFieldFunc = function() {
        this.view.render({
            func_list: this.getFieldFuncList()
        });
    };
    /**
     * Возвращает список должностей отдела выбранного в форме
     */
    this.getFieldFuncList = function() {
        var func_list = FuncModel.getList();
        var place_number = this.field_list.place.val();
        if (typeof place_number == 'string') {
            place_number = parseInt(place_number);
        }
        else {
            place_number = 1;
        }
        var place = PlaceModel.getByNumber(place_number);
        if (place != null && place.func.length > 0) {
            var func_list_temp = func_list;
            func_list = [];
            for (var i in func_list_temp) {
                if ($.inArray(func_list_temp[i].getNumber(), place.func) >= 0 ) {
                    func_list[i] = func_list_temp[i];
                }
            }
        }
        return func_list;
    };
};