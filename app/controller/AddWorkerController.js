/**
 * Контроллер выполняет какие либо монипуляции с данными и выводит их через Представление
 * Контроллер отвечает за добавление нового работника и вывод формы добавления
 * @property view Представление контроллера
 * @type {AddWorkerController}
 */
var AddWorkerController = new function() {
    var t = this;
    /** @type AddWorkerView */
    this.view = null;

    this.worker = null;// Новый работник

    this.btn = null;//Кнопка на понели меню

    this.field_list = {
        name:null,
        surname:null,
        patronymic:null,
        date:null,
        func:null,
        place:null,
        phone:null,
        status:null
    };

    this.init = function() {
        ViewComponent.getView("AddWorker", this, function() {
            //t.btn.click();
            //console.debug(t.field_list);
        });
        $('#menu')
            .append(this.btn = $('<button>', {type: 'button', 'class': "btn btn-default"})
                .html("Добавить")
                .click(function() {
                    t.click();
                })
            );
    };

    this.open = function() {
        this.view.open();
        this.initField();
    };
    this.close = function() {
        this.view.close()
    };
    this.click = function() {
        this.open();
    };

    /**
     * Сохраняет нового работника и проверяет валидность его данных
     */
    this.save = function() {
        var data = {};
        for (var i in this.field_list) {
            data[i] = this.field_list[i].val()
        }
        data.place = parseInt(data.place);
        data.func = parseInt(data.func);
        this.worker = WorkerModel.createModel(WorkerModel.getNewId(), data);

        var error_list = this.worker.check();

        if (error_list === true) {
            this.worker.save();
            this.view.close();
            TableController.update();
        }
        else {
            for (var i in error_list) {
                this.view.fieldSetError(error_list[i].field, error_list[i].mess);
            }
        }

    };
    /**
     * Открывает форму добавления нового работника
     */
    this.initField = function() {
        var data = {
            place_list: PlaceModel.getList(),
            func_list: this.getFieldFuncList(),
            status_list: [
                {
                    value: "work",
                    name: "Работает"
                },
                {
                    value: "fired",
                    name: "Уволен"
                }
            ]
        };
        this.view.render(data);
    };

    /**
     * При изменении отдела в форме нужно загрузить список должностей для этого отдела
     */
    this.refreshFieldFunc = function() {
        this.view.render({
            func_list: this.getFieldFuncList()
        });
    };
    /**
     * Возвращает список должностей отдела выбранного в форме
     */
    this.getFieldFuncList = function() {
        var func_list = FuncModel.getList();
        var place_number = this.field_list.place.val();
        if (typeof place_number == 'string') {
            place_number = parseInt(place_number);
        }
        else {
            place_number = 1;
        }
        var place = PlaceModel.getByNumber(place_number);
        if (place != null && place.func.length > 0) {
            var func_list_temp = func_list;
            func_list = [];
            for (var i in func_list_temp) {
                if ($.inArray(func_list_temp[i].getNumber(), place.func) >= 0 ) {
                    func_list[i] = func_list_temp[i];
                }
            }
        }
        return func_list;
    };
};
