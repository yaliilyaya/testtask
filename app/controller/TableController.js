/**
 * Контроллер выполняет какие либо монипуляции с данными и выводит их через Представление
 * Контроллер отвечает за вывод информации о работниках в таблицу
 * Имеет дополнительный функционал:
 * 1) фильтрация результатов
 * 2) сортировка
 * 3) Постраничная навигация
 * Так же задаёт для каждого работника кнопки смены статуса и перемещения между отделами
 * @property view Представление контроллера
 * @type {TableController}
 */
var TableController = new function() {
    var t = this;
    this.view = "Table";
    this.list_row = [];
    this.filter = {
        page: 1,
        limit: 5
    };
    this.sort = null;
    this.init = function() {
        ViewComponent.getView("Table", this, function() {
            t.setSort("fio");
        });
    };
    /**
     * Обновляет данные в таблице по ряду параметров
     */
    this.update = function() {
        this.list_row = WorkerModel.getList(this.filter, this.sort);

        this.func_list = StorageComponent.getFuncList();
        this.place_list = StorageComponent.getPlaceList();

        for (var i in this.list_row) {
            var worker = this.list_row[i];
            var func_id = "func_" + worker.func; //Заменяем ID должности на имя
            var place_id = "place_" + worker.place;//Заменяем ID отдела на имя

            worker.func_name = (this.func_list[func_id] != null) ? this.func_list[func_id].name : "";
            worker.place_name = (this.place_list[place_id] != null) ? this.place_list[place_id].name : "";
        }

        this.view.render({
            worker_list: this.list_row,
            //данные для постраничной навигации
            count_page: Math.ceil(WorkerModel.getCount(this.filter) / this.filter.limit),
            page: this.filter.page
        })
    };
    /**
     * Задаём страницу в таблице
     * @param page
     */
    this.changePage = function(page) {
        this.filter.page = page;
        this.update();
    };
    /**
     * Задаём сортировку в таблице
     * @param type
     */
    this.setSort = function(type) {
        if (type === "fio") {
            this.sort = function (a, b) {
                return t.sortField('fio', a, b) || t.sortField('place', a, b)|| t.sortField('func', a, b);
            };
        }
        else if (type === "place") {
            this.sort = function (a, b) {
                return t.sortField('place', a, b) || t.sortField('fio', a, b)|| t.sortField('func', a, b);
            };
        }
        else if (type === "status") {
            this.sort = function (a, b) {
                return t.sortField('status', a, b) || t.sortField('fio', a, b) || t.sortField('place', a, b)|| t.sortField('func', a, b);
            };
        }
        this.update();
    };
    /**
     * Задаём функцию фильтрации в таблице
     * @param check_func
     */
    this.setFilter = function(check_func) {
        this.filter.check = check_func;
        this.update();
    };
    /**
     * Метод для сортировки отдельного поля в объекте
     * @param field
     * @param a
     * @param b
     * @returns {*}
     */
    this.sortField = function(field, a, b) {
        if (field == "fio") {
            return WorkerModel.getFullName(a).localeCompare(WorkerModel.getFullName(b));
        }
        else if ($.inArray(field, ["status"]) >= 0) {

            return a[field].localeCompare(b[field]);
        }
        return a[field] - b[field];

    }
};
