/**
 * Обьект помогает загрузить Предстовление или Шаблон с сервера
 * @type {ViewComponent}
 */
var ViewComponent = new function ( ) {
    /**
     * Загружает представление и связывает его с контроллером
     * Запускает метод init() для инициализации представления
     * @param name
     * @param controller
     * @param callback
     */
    this.getView = function(name, controller, callback) {
        $.getScript('/app/view/'+name+"View.js", function() {
            controller.view = new window[name+ "View"]();
            if (controller.view['init'] != null) {
                controller.view.controller = controller;
                controller.view.init();
                callback()
            }
        });
    };
    this.getTemplate = function(name, callback) {
        $.get('/app/template/'+name+"Template.tpl", {}, function(d) {
                callback(d)
        });
    };
};