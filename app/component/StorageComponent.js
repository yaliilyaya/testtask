/**
 * Класс для удобного обращения к хранилищу данных.
 * содержит определение используемого хранилища
 * Проверяет хранилище если оно пустое то загружает в него данные из файла на сервере
 * @type {StorageComponent}
 */
var StorageComponent = new function() {
    var t = this;
    this.storage = $.localStorage;

    this.init = function() {
        //загрузка данных из файла
        if (this.storage.isEmpty('place_list') || this.storage.isEmpty('worker_list') || this.storage.isEmpty('func_list')) {
            $.getJSON('/app/data.json', null, function(data) {
                if (t.storage.isEmpty('place_list') || t.storage.isEmpty('func_list')) {
                    t.storage.set('place_list', data['place_list']);
                    t.storage.set('func_list', data['func_list']);
                }
                else if (t.storage.isEmpty('worker_list')) {
                    t.storage.set('worker_list', data['worker_list']);
                }
                window.location.href = "";
                //TODO:: Проблема при инициализации хранилища
                //t.storage.remove(['place_list', 'worker_list', 'func_list']);
            });
        }
        else {
            //this.storage.remove(['place_list', 'worker_list', 'func_list']);
            console.debug(this.storage.get(['place_list', 'worker_list', 'func_list']));
        }
    };
    /**
     * @returns {object}
     */
    this.getPlaceList = function() {
        return this.storage.get('place_list')
    };
    /**
     * @returns {object}
     */
    this.getWorkerList = function() {
        return this.storage.get('worker_list')
    };
    /**
     * @returns {object}
     */
    this.getFuncList = function() {
        return this.storage.get('func_list')
    };
    /**
     * Осуществляет поиск работника по его ID
     * @returns object
     */
    this.getWorker = function(id) {
        return this.storage.get('worker_list.' + id);
    };
    /**
     * Осуществляет поиск Должности по его ID
     * @returns object
     */
    this.getPlace = function(id) {
        return this.storage.get('place_list.' + id);
    };
    /**
     * Обновляет данные или сохраняет нового работника в хранилище
     * @param id
     * @param val
     */
    this.setWorker = function(id, val) {
        this.storage.set('worker_list.'+ id, val);
    }

};