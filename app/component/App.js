/**
 * Главный объек Приложение.
 * Загружает компоненты, контроллеры и модели
 * @type {App}
 */
var App = new function() {
    this.init = function() {

        this.loadComponent('View');
        this.loadComponent('Validator');
        this.loadComponent('Storage');


        this.loadModel('Worker');
        this.loadModel('Place');
        this.loadModel('Func');

        this.loadController('Table');
        this.loadController('Search');
        this.loadController('AddWorker');
        this.loadController('ChangePlace');
        this.loadController('Info');
    };
    /**
     * Загружает компонент по имени
     * Запускает метод init() для инициализации компонента
     * @param name
     */
    this.loadComponent = function(name) {
        name += "Component";
        $.getScript('/app/component/'+name+".js", function() {
            if (window[name]['init'] != null) {
                window[name].init();
            }
        });
    };
    /**
     * Загружает контроллер по имени
     * Запускает метод init() для инициализации контроллера
     * @param name
     */
    this.loadController = function(name) {
        name += "Controller";
        $.getScript('/app/controller/'+name+".js", function() {
            if (window[name]['init'] != null) {
                window[name].init();
            }
        });
    };
    /**
     * Загружает модель по имени
     * @param name
     */
    this.loadModel = function(name) {
        name += "Model";
        $.getScript('/app/model/'+name+".js");
    };
};
App.init();