/**
 * Создаёт обьекты проверки разных типов данных.
 * Каждый объект проверки содержит метод check() для проверки корректности значения
 * и метод mess() для выдачи сообщения пользователю
 * @type {ValidatorComponent}
 */
var ValidatorComponent = new function() {
    this.text = function(val) {
        return new ValidatorText(val);
    };
    this.date = function() {
        return new ValidatorDate();
    };
    this.phone = function() {
        return new ValidatorPhone();
    };
};
/**
 * Проверяет длинну и наличие символов отличных от русского алфовита
 * @param limit
 * @constructor
 */
function ValidatorText(limit) {
    this.limit = limit;
    this.check = function(val) {
        if (val.search(/[^а-яА-я]/) != -1) {

            return false;
        }
        return (val.length <= this.limit);
    };
    this.mess = function() {
        return "Значение привышает " + this.limit + " символов или содержит недоступные символы. " +
            "Допустимы только буквы русского алфавита";
    };
}
/**
 * Проверяет дату по маске
 * @constructor
 */
function ValidatorDate() {
    this.check = function(val) {
        return (val.search(/\d{1,2}\.\d{1,2}\.\d{4}/) != -1);
    };
    this.mess = function() {
        return "Неправильный формат даты. Пример паравельного формата 1.12.1988";
    };
}
/**
 * Проверяет телефон по маске
 * @constructor
 */
function ValidatorPhone() {
    this.check = function(val) {

        return (val.search(/\+7\(\d{3}\)\s\d{3}\s\d{2}\s\d{2}/) != -1);
    };
    this.mess = function() {
        return "Неправильный формат телефона. Пример паравельного формата +7(666) 666 66 66";
    };
}