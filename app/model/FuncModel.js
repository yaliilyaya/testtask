/**
 * Модель реальзует доступ сущности Должность к хранилищу
 */
var FuncModel = new function FuncModelBase() {
    this.id = null;
    this.name = null;

    /**
     * Задаёт данные модели
     * @param data
     */
    this.setAttr = function(data) {
        for (var i in data) {
            this[i] = data[i];
        }
    };
    /**
     * @param i
     * @param val
     * @returns {*}
     */
    this.set = function(i , val) {
        return this[i] = val;
    };
    /**
     * Возвращает числовой ID должности
     * @returns {*}
     */
    this.getNumber = function() {
        var number = this.id.replace(/[a-z_-]+/, "");
        number = parseInt(number);
        return (isNaN(number) ? 0 : number);
    };
    /********** API ************/
    /**
     * Создаёт новую модель с данными об должности
     * @param id
     * @param data
     * @returns {FuncModelBase}
     */
    this.createModel = function(id, data) {
        var model = new FuncModelBase;
        model.id = id;
        model.setAttr(data);
        return model
    };
    /**
     * Возвращает модель должности с его данными
     * @param val
     * @returns {FuncModelBase}
     */
    this.getByNumber = function(val) {
        var func_data = StorageComponent.getFuncList('func_' + val);
        return this.createModel('func_' + val, func_data);
    };
    /**
     * Возвращает список должностей
     * @returns {Array}
     */
    this.getList = function() {
        var list = StorageComponent.getFuncList(),
            list_return = [];
        for(var i in list) {

            list_return[i] = this.createModel(i, list[i]);

        }
        return list_return;
    };
    /**
     * Выдаёт колличество должностией в хранилище
     * имеет зависимость от функции фильтрации
     * @returns {number}
     */
    this.getCount = function() {
        var list = StorageComponent.getFuncList(),
            count = 0;
        for(var i in list) {
            count++;
        }
        return count;
    };

};