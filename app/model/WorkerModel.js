/**
 * Модель реальзует доступ сущности Работник к хранилищу
 * Каждая модель реализующая отдельно взятого работника имеет методы для работы с ним.
 * Также имеется проверка полей на валидность.
 */
var WorkerModel = new function WorkerModelBase() {
    this.id = null;
    this.name = null;
    this.surname = null;
    this.patronymic = null;
    this.date = null;
    this.func = null;
    this.place = null;
    this.phone = null;
    this.status = null;

    /**
     * Задаём валидаторы для полей
     */
    this.validate = {
        name: ValidatorComponent.text(15),
        surname: ValidatorComponent.text(15),
        patronymic: ValidatorComponent.text(100),
        date: ValidatorComponent.date(),
        phone: ValidatorComponent.phone()
    };
    /**
     * Задаём обязательные поля
     */
    this.require = [
        "name", "surname", "date", "phone"
    ];

    /**
     * Задаёт данные модели
     * @param data
     */
    this.setAttr = function(data) {
        for (var i in data) {
            this[i] = data[i];
        }
    };
    /**
     * @param i
     * @param val
     * @returns {*}
     */
    this.set = function(i , val) {
        return this[i] = val;
    };
    /**
     * Осуществляет проверку данных работника на валидность
     * Возврящает true в случае положительной проверки,
     * иначе возвращает массив с сообщениями об ошибках
     * @returns {*}
     */
    this.check = function() {
        var error = [];
        for (var i in this.require) {
            if (this[this.require[i]].length <= 0) {
                error.push({
                    field: this.require[i],
                    mess: "Незаполненно обязательное поле"
                });
            }
        }
        if (error.length > 0) {
            return error;
        }
        for (var i in this.validate){
            if (!this.validate[i].check(this[i])) {
                error.push({
                    field: i,
                    mess: this.validate[i].mess()
                });
            }
        }
        return (error.length > 0 ? error : true);
    };


    /**
     * Сохраняет данные работника в хранилище
     */
    this.save = function() {
        var save_obj = {};
        var field_name = ['name', 'surname', 'patronymic', 'date', 'func', 'place', 'phone', 'status'];
        for (var i in field_name) {
            save_obj[field_name[i]] = this[field_name[i]];
        }
        //console.debug([this.id, save_obj]);
        StorageComponent.setWorker(this.id, save_obj);
    };
    /**
     * Возвращает числовой ID работника
     * @returns {*}
     */
    this.getNumber = function() {
        var number = this.id.replace(/[a-z_-]+/);
        number = parseInt(number);
        return (isNaN(number) ? 0 : number);
    };

    /**
     * Задаёт статус работнику Работает
     */
    this.setStatusWork = function() {
        this.status = "work";
        this.save();
    };
    /**
     * Задаёт статус работнику Уволен
     */
    this.setStatusFired = function() {
        this.status = "fired";
        this.save();
    };
    /**
     * @returns {string}
     */
    this.fullName = function() {
        return this.getFullName(this);
    };

    /********** API ************/
    /**
     * Создаёт новую модель с данными о работнике
     * @param id
     * @param data
     * @returns {WorkerModelBase}
     */
    this.createModel = function(id, data) {
        var model = new WorkerModelBase;
        model.id = id;
        model.setAttr(data);
        return model
    };
    /**
     * Выдаёт ID для нового работника
     * @returns {string}
     */
    this.getNewId = function() {
        var key_list = StorageComponent.storage.keys('worker_list');
        for(var i in key_list) {
            key_list[i] = parseInt(key_list[i].replace(/[a-z_]+/, ''));
        }
        function arrayMax(numArray) {
            return Math.max.apply(null, numArray);
        }
        return "worker_" + (arrayMax(key_list) + 1);
    };

    /**
     * Возврящает модель с данными о работнике по ID
     * @param id
     * @returns {*}
     */
    this.getWorker = function(id) {
        id = id + "";
        if (id.length <= 0) {
            return null;
        }
        var data = StorageComponent.getWorker(id);
        if (typeof data != "object") {
            return null;
        }
        return this.createModel(id, data);
    };
    /**
     * Возвращает список работников из хранилища
     * имеет фильтрацию и сортировку результатов
     * @param filter
     * @param sort
     * @returns {Array}
     */
    this.getList = function(filter, sort) {
        filter = this.checkFilter(filter);
        var list = StorageComponent.getWorkerList(),
            list_return = [],
            count = 0,
            index = 0;

        list = this.convertToArray(list);
        if (typeof sort == "function") {
            list.sort(sort);
        }
        for(var i in list) {

            if (++index < (filter.page * filter.limit + 1)) {
                continue;
            }
            if (!filter.check(list[i])) {
                continue;
            }
            list_return[i] = this.createModel(list[i].id, list[i]);
            if (++count >= filter.limit) {
                break;
            }
        }
        return list_return;
    };

    /**
     * Корректно преобразовывает список объекто с данными о работниках в массив
     * @param worker_list
     * @returns {Array}
     */
    this.convertToArray = function(worker_list) {
        var list = [];
        for (var i in worker_list) {
            worker_list[i].id = i;
            list.push(worker_list[i]);
        }
        return list;
    };
    /**
     * Выдаёт колличество пользователей в хранилище
     * имеет зависимость от функции фильтрации
     * @param filter
     * @returns {number}
     */
    this.getCount = function(filter) {
        filter = this.checkFilter(filter);
        var list = StorageComponent.getWorkerList(),
            count = 0;
        for(var i in list) {
            if (!filter.check(list[i])) {
                continue;
            }
            count++;
        }
        return count;
    };
    /**
     * Проверяет фильтр и/или задаёт значения по умолчанию
     * @param filter
     * @returns {{limit: number, page: number, check: *}}
     */
    this.checkFilter = function(filter) {
        if (typeof(filter) != "object" ) {
            filter = {};
        }
        return {
            limit: ((filter['limit'] == null || filter.limit < 1) ? 1 : filter.limit),
            page: ((filter['page'] == null || filter.page < 1) ? 0 : filter.page - 1),
            check: (filter['check'] == null ? function(){return true} : filter.check)
        };
    };

    /**
     * @param worker
     * @returns {string}
     */
    this.getFullName = function(worker) {
        return worker.surname + " " + worker.name + " " +  worker.patronymic
    };

};