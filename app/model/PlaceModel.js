/**
 * Модель реальзует доступ сущности Отдел к хранилищу
 */
var PlaceModel = new function PlaceModelBase() {
    this.id = null;
    this.name = null;
    this.func = [];
    this.worker = [];

    /**
     * Задаёт данные модели
     * @param data
     */
    this.setAttr = function(data) {
        for (var i in data) {
            this[i] = data[i];
        }
    };
    /**
     * @param i
     * @param val
     * @returns {*}
     */
    this.set = function(i , val) {
        return this[i] = val;
    };
    /**
     * Возвращает числовой ID отдела
     * @returns {*}
     */
    this.getNumber = function() {
        var number = this.id.replace(/[a-z_-]+/, "");
        number = parseInt(number);
        return (isNaN(number) ? 0 : number);
    };
    /********** API ************/
    /**
     * Создаёт новую модель с данными об отделе
     * @param id
     * @param data
     * @returns {PlaceModelBase}
     */
    this.createModel = function(id, data) {
        var model = new PlaceModelBase;
        model.id = id;
        model.setAttr(data);
        return model
    };


    /**
     * Возвращает модель отдела с его данными
     * @param val
     * @returns {PlaceModelBase}
     */
    this.getByNumber = function(val) {
        var place_data = StorageComponent.getPlace('place_' + val);
        return this.createModel('place_' + val, place_data);
    };
    /**
     * Возвращает список отделов
     * @returns {Array}
     */
    this.getList = function() {
        var list = StorageComponent.getPlaceList(),
            list_return = [];
        for(var i in list) {
            list_return[i] = this.createModel(i, list[i]);
        }
        return list_return;
    };
    /**
     * Возвращает колличество отделов в списке
     * имеет фильтрацию
     * @param filter
     * @returns {*}
     */
    this.getCount = function() {

        var list = StorageComponent.getPlaceList(),
            count = 0;
        for(var i in list) {
            count++;
        }
        return count;
    };

    /**
     * Проверяет фильтр и/или задаёт значения по умолчанию
     * @param filter
     * @returns {{limit: number, page: number, check: *}}
     */
    this.checkFilter = function(filter) {
        if (typeof(filter) != "object" ) {
            filter = {};
        }
        return {
            limit: ((filter['limit'] == null || filter.limit < 1) ? 1 : filter.limit),
            page: ((filter['page'] == null || filter.page < 1) ? 0 : filter.page - 1),
            check: (filter['check'] == null ? function(){return true} : filter.check)
        };
    };
}
